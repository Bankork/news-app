var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');

server.listen(8890);


io.on('connection', function (socket) {


    console.log("new client connected : 8890");

    var redisClient = redis.createClient();
    redisClient.subscribe('message', function (err, count) {
        if (err && typeof err != undefined) {
            console.log('NEWS-POST: ' + JSON.stringify(err));
        }
    });
    
    redisClient.subscribe('news-post', function (err, count) {
        if (err && typeof err != undefined) {
            console.log('NEWS-POST: ' + JSON.stringify(err));
        }
    });

    redisClient.on("message", function (channel, message) {
        message = JSON.parse(message);
//        console.log("New message in broadcast queue " + message.data + '. Active on channel ' + channel);
//        console.log("New message in broadcast queue " + JSON.stringify(message) + '. Active on channel ' + channel);
//        
	if(channel == "news-post"){
            console.log("New message in broadcast Data: " + message.data + '. Active on channel ' + channel);
            socket.emit(channel, message.data);
	}
        if(channel == "message"){
             console.log("New message in broadcast Data: " + message.data + '. Active on channel ' + channel);
            socket.emit(channel + ':' + message.event, message.data);
	}
        //socket.emit(channel, message);
    });

    socket.on('disconnect', function () {
        redisClient.quit();
    });


});

