<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class GenAccessTokenRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('bankork.gen_access_token.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
