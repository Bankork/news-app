<?php

namespace App\Api\V1\Controllers;

use JWTAuth;
use Auth;
use App\User;
use App\ApiCallLog;
use App\Client;
use Storage;
//use File;
use LRedis;
use Helper;
use App\NewsPost;
use App\NewsFile;
use App\NewsLike;
use App\NewsComment;
use App\NewsChannel;
use App\UserChannel;

use App\Events\NewsPostEvent;

use Validator;
//use GeoIP;
use Event;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
//use Tymon\JWTAuth\JWTAuth;
use App\Api\V1\Requests\LoginRequest;
use App\Api\V1\Requests\GenAccessTokenRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ImagineController extends Controller {

    use Helpers;

    protected $client;

    public function __construct() {
        $this->client = "";
    }

    /**
     * Check current User token
     * @return bool : to check the validity of the user token
     */
    private function currentUser() {
        $res = JWTAuth::parseToken()->authenticate();
        return $res;
    }

    public function testinsert() {
        //$CU = User::createOrUpdate(['name'=> "John Agbaje", 'password' => bcrypt('secret')],['email' => 'bade']);
        //$id = $CU->id;
        echo "res - ";
    }

    /**
     * Authenticate User
     * @param Request $request
     * @return type
     */
    public function authenticate(LoginRequest $request) {

        $utoken = $request->get('utoken');
        $userid = $request->get('userid');
        $password = $request->get('password');

        $token = "";
        $userdata = [];
        $data_mapped = false;
        $bcauthorize = "";
        $bctoken = "";
        $bcusername = "";
        $bcr_url = "";

        //------ build FBN_DRC request --------- //
        $paramsreg = array("Password" => $password,
            "TokenCode" => $utoken,
            "UserID" => $userid);
        $params = array('request' => $paramsreg);


        //---- call to FBN-DRC -----//
        $objectresult = $this->client->Authentication($params);
        $response_code = $objectresult->AuthenticationResult->ResponseCode;
        $response_desc = $objectresult->AuthenticationResult->ResponseDescription;


        //------ if call is successful --------- //
        if ($response_code == 0) {

            $BranchCode = $objectresult->AuthenticationResult->BranchCode;
            $Role = $objectresult->AuthenticationResult->Role;
            $TellerTillAccount = $objectresult->AuthenticationResult->TellerTillAccount;

            $userdata = [ 'userid' => $userid,
                'Email' => $objectresult->AuthenticationResult->Email,
                'FirstName' => $objectresult->AuthenticationResult->FirstName,
                'LastName' => $objectresult->AuthenticationResult->LastName,
                'bankBranchCode' => $BranchCode,
                'bankRole' => $Role,
                'bankTillAccount' => $TellerTillAccount];

            $name = $objectresult->AuthenticationResult->LastName . " " . $objectresult->AuthenticationResult->FirstName;

            //--------- insert if not exist ------- //
            $CU = User::createOrUpdate(['name' => $name, 'password' => bcrypt($password)], ['email' => $userid]);
            $cuID = is_object($CU) ? $CU->id : 0;

            //----------- generate token ------------- //
            $tokenresponse = $this->generateToken($request, $cuID);
            $tokenresponse = json_decode($tokenresponse->getContent());
            if ($tokenresponse->response_code == "00") {
                $token = $tokenresponse->token;
            }

            //------ if record has been mapped on Branchcollect ----- //
            $user = $this->getUser();
            $data_mapped = ($user->is_mapped == 1) ? true : false;
            $bcusername = ($data_mapped) ? $user->bcusername : "";

            //------ for BC JWT ----------- //
            $tokenrequest = $this->bcRequestJWT();
            $tokenrequest = json_decode($tokenrequest);
            $bctoken = $tokenrequest->token;
//
//                //---- call to authenticate BC user ------- //
//                if($data_mapped){
//                    //$bctoken = $tokenrequest->token; //token should be passes in header as: BCtoken: $token
//                    $bcauthparams = array(  'bcusername' => $bcusername,
//                                            'bankRole' => $Role,   
//                                            'bankBranchCode' => $BranchCode,
//                                            'bankTillAccount' => $TellerTillAccount);
//                    $bcauthorize = $this->bcAuthorization($bcauthparams,$bctoken);
//                    $bcauthorize = json_decode($bcauthorize);
//                }
            if ($data_mapped) {
                $bcr_url = env('BC_REDIRECT_URL');
                $bcauthparams = array('bcusername' => $bcusername,
                    'bankRole' => $Role,
                    'bankBranchCode' => $BranchCode,
                    'bankTillAccount' => $TellerTillAccount);

                $bcauth_res = $this->doBcAuthorization($bcauthparams, $bctoken);
                $bcauth_res = json_decode($bcauth_res);
                $bcauthorize = $bcauth_res->bcauthorize;
            }
        }
        //------- build client response ----- //
        $client_response = [ 'response_code' => $response_code,
            'response_description' => $this->getTransMsg($response_desc),
            'token' => $token,
            'utoken' => $utoken,
            'bctoken' => "$bctoken",
            'data_mapped' => $data_mapped,
            'bcusername' => $bcusername,
            'bcauthorize' => $bcauthorize,
            'bcr_url' => $bcr_url,
            'data' => [$userdata]];
// echo "<pre>". print_r($client_response,1). print_r($objectresult,1);
//return;           
        //------- build log parameters ----- //
        $logparams['method'] = "Authenticate";
        $logparams['sent2client'] = $client_response;
        $this->logApiCall($logparams, $request, false);

        return response()->json($client_response, 200);
    }

    /**
     * Query Single Debit Single Credit
     * @param Request $request
     * @return type
     */
    public function userchannel(Request $request) {

        if ($this->currentUser()) {

            //------- build client response ----- //
            $client_response = [ 'response_code' => $response_code,
                'response_description' => $this->getTransMsg($response_desc),
                'data' => ['batchid' => $batchid, 'transaction_ref' => $trans_ref]];

            //------- build log parameters ----- //
            $logparams['method'] = "QuerySingleDebitSingleCredit";
            $logparams['sent2client'] = $client_response;
            $this->logApiCall($logparams, $request);

            return response()->json($client_response, 200);
        } else {
            $err_auth = $this->getTransMsg('could_not_authenticate_user');
            return $this->response->error($err_auth, 500);
        }
    }

    /**
     * 
     * Log all API Calls
     * @param array $params : array of parameters of calls to log
     * @param object $request : array of request sent
     * @param bool $validate
     * @param int $client_id
     */
    private function logApiCall($params, $request, $validate = true, $client_id = 0) {

        $apicall = new ApiCallLog;

        $apicall->log_ip = $request->ip();
        $apicall->log_method = $params['method'];
        $apicall->log_request = json_encode($request->except(['password', 'clientKey'])); //json_encode($request->all());
        $apicall->log_request_original = "";
        $apicall->log_response = "";
        $apicall->log_response_sent2client = json_encode($params['sent2client']);
        $apicall->log_response_updated = "1";

        //---- log the call ------------ //
        if ($validate) {
            $this->currentUser()->apiCallLogs()->save($apicall);
        } elseif (!is_null($this->getUser())) {
            $user = $this->getUser();
            $apicall->log_client_id = $user->id;
            $apicall->save();
        } else {
            $apicall->log_client_id = 0;
            $apicall->save();
        }
    }

    private function logApiCall2($params, $request, $validate = true, $client_id = 0) {

        $apicall = new ApiCallLog;

        $apicall->log_ip = $request->ip();
        $apicall->log_method = $params['method'];
        $apicall->log_request = json_encode($request->except(['password', 'clientKey'])); //json_encode($request->all());
        $apicall->log_request_original = "";
        $apicall->log_response = "";
        $apicall->log_response_sent2client = json_encode($params['sent2client']);
        $apicall->log_response_updated = "1";

        //---- log the call ------------ //
        if ($validate) {
            $this->currentUser()->apiCallLogs()->save($apicall);
        } elseif (!is_null($this->getUser())) {

            $user = $this->getUser();
            $apicall->log_client_id = $user->id;
            $apicall->save();
        } else {
            $apicall->log_client_id = 0;
            $apicall->save();
        }
    }

    private function change_key($array, $old_key, $new_key) {

        if (!array_key_exists($old_key, $array))
            return $array;

        $keys = array_keys($array);
        $keys[array_search($old_key, $keys)] = $new_key;

        return array_combine($keys, $array);
    }

    /**
     * Vlaidate Client
     * @param type $request
     * @return boolean
     */
    private function checkClient(GenAccessTokenRequest $request) {

        $userId = $request->get('username');
        $consumerKey = $request->get('clientKey');
        $userGeneratedUniqueId = $request->get('uniqueId');
        $user_mac = $request->get('mac');
        $expected_mac = hash('sha256', "$consumerKey|$userId|$userGeneratedUniqueId");
        $response = false;

        //----  verify mac ---- //
        if ($expected_mac == $user_mac)
            $response = true;

        return $response;
    }

    /**
     * Generate Token for Client
     * @param type $request
     * @return type
     * @throws HttpException
     * @throws AccessDeniedHttpException
     */
    public function generateAccessToken(GenAccessTokenRequest $request) {
        $clientkey = $request->get('clientKey');
        $userId = $request->get('username');
        $mac = $request->get('mac');
        $response = false;
        $user_response = false;
        $msg = "Invalid client key";

        //---- verify user details --- //
        $check_user_rec = User::where('bcusername', $userId)->where('is_mapped', "1")->get();
        if ($check_user_rec->count())
            $user_response = true;
        else
            $msg = "Invalid user supplied";


        //---- verify client --- //
        $check_rec = Client::where('client_secret_key', $clientkey)->get();
        if ($check_rec->count() && ($user_response == true)) {
            //--- validate mac ----- //
            $response = $this->checkClient($request);
            $msg = $response ? "" : "Invalid mac address";
        }

        //----- if response is false --- //
        if (!$response) {
            //------- build log parameters ----- //
            $clientres = [
                'response_code' => '01',
                'response_description' => $this->getTransMsg($msg),
            ];
            $logparams['method'] = "GenerateAccessToken";
            $logparams['sent2client'] = $clientres;
            $this->logApiCall2($logparams, $request, false);

            return response()->json($clientres);
        }

        $credentials['id'] = $check_user_rec[0]->id;
        $customClaims = ['mac' => $mac, 'ckey' => $clientkey];


        try {
            $token = JWTAuth::attempt($credentials, $customClaims, true);

            if (!$token) {
                throw new AccessDeniedHttpException();
            }
        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        //------- build client response ----- //
        $client_response = [
            'response_code' => '00',
            'response_description' => $this->getTransMsg('Success'),
            'token' => $token
        ];

        //------- build log parameters ----- //
        $logparams['method'] = "GenerateAccessToken";
        $logparams['sent2client'] = $client_response;
        $this->logApiCall2($logparams, $request, false);

        return response()->json($client_response);
    }

    /**
     * 
     * @param type $request
     * @return type
     * @throws HttpException
     * @throws AccessDeniedHttpException
     */
    private function generateToken(LoginRequest $request, $id = 0) {

        $credentials = $request->only(['userid', 'password']);
        $credentials = $this->change_key($credentials, 'userid', 'email');

        try {
            $token = JWTAuth::attempt($credentials);
            if (!$token && $id != 0) {
                $credentials['id'] = $id;
                $token = JWTAuth::attempt($credentials, [], true);
            }
            if (!$token)
                throw new AccessDeniedHttpException();
        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        //------- build client response ----- //
        $client_response = [
            'response_code' => '00',
            'response_description' => $this->getTransMsg('Success'),
            'token' => $token
        ];

        //------- build log parameters ----- //
        $logparams['method'] = "GenerateToken";
        $logparams['sent2client'] = $client_response;
        $this->logApiCall2($logparams, $request, false);

        return response()->json($client_response);
    }

    private function getUser() {
        $user = Auth::user();
        return $user;
    }

    /**
     * Word Translation
     * @param string $msg 
     * @param array $toreplace
     * @param string $convertto
     * @return string
     */
    private function getTransMsg($msg, $toreplace = [], $convertto = 'fr') {
        $convertedmsg = _t($msg, $toreplace, $convertto);
        return $convertedmsg;
    }

    //======================================================== NEW CHANNEL API ======================================
    public function post(Request $request, $id) {
        //----- get users channel ---- //
        $user = User::find($id);
        //dd($user->userChannel);

        $uc = User::getchannel($id);
        //dd($uc);
        $posts = NewsPost::with('news_files', 'nchannel')->where('status', 1)
                        ->where('completed_post_process', true)
                        ->whereIn('news_channel_id', $uc)
                        ->take(500)->orderBy('id', 'desc')->get();
        $posts->load([
            'user' => function($query) {
                $query->select('users.id', 'users.name', 'users.lastname', 'users.slug');
            },
            'comments' => function($query) {
                $query->select('users.name', 'users.lastname', 'users.slug', 'news_comments.*')// 'news_comments.comment_by', 'news_comments.news_post_id', 'news_comments.comment')
                        ->join('users', 'news_comments.comment_by', '=', 'users.id')
                        ->where('news_comments.status', true);
            },
            'news_likes' => function($query) {
                $query->select('news_likes.id', 'news_likes.news_post_id', 'news_likes.liked_by');
            }]);
//            dd($posts);
        //return JSON Object
        return response()->json($posts);
    }

    
    public function updatepost(Request $request, $postid) {
        
        $posts = NewsPost::with('news_files', 'nchannel')->where('status', 1)
                        ->where('completed_post_process', true)
                        ->where('id', $postid)
                        ->take(500)->orderBy('id', 'desc')->get();
        $posts->load([
            'user' => function($query) {
                $query->select('users.id', 'users.name', 'users.lastname', 'users.slug');
            },
            'comments' => function($query) {
                $query->select('users.name', 'users.lastname', 'users.slug', 'news_comments.*')// 'news_comments.comment_by', 'news_comments.news_post_id', 'news_comments.comment')
                        ->join('users', 'news_comments.comment_by', '=', 'users.id')
                        ->where('news_comments.status', true);
            },
            'news_likes' => function($query) {
                $query->select('news_likes.id', 'news_likes.news_post_id', 'news_likes.liked_by');
            }]);
//            dd($posts);
        //return JSON Object
        return response()->json($posts);
    }
    
    
    public function mychannel(Request $request) {
        $id = 1;
        $uchannels = User::getchannel($id, false);
        foreach ($uchannels as $uchannel) {
            echo $uchannel->news_channel;
        }
        dd($uchannels);
    }
    
    public function sendMessage(Request $request) {

        //---- save post ----- //
        $news = new NewsPost();
        $news->user_id = '1';
        $news->news_channel_id = $request->get('channel');
        $news->slug = $request->get('slug');
        $news->post = $request->get('message');
        $news->status = true;
        $news->completed_post_process = true;
        $news->save();
        
//        $redis = LRedis::connection();
//        $redis->publish('news-post', $this->post($request,'1'));
        $rec = $this->updatepost($request,$news->id);
        //fire event
        Event::fire(new NewsPostEvent($rec->getdata()));//$approved_post->toJson())

        return redirect('writemessage');
    }

}
