<?php

namespace App\Api\V1\Controllers;

use App\User;
use Symfony\Component\HttpKernel\Exception\HttpException;
//use Tymon\JWTAuth\JWTAuth;
use App\Http\Requests;
use Illuminate\Http\Request;
use JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LoginController extends Controller {

    public function login(LoginRequest $request, $id = 0) {

        $credentials = $request->only(['email', 'password']);


        try {
            $token = JWTAuth::attempt($credentials);
            if (!$token && $id != 0) {
                $credentials['id'] = $id;
                $token = JWTAuth::attempt($credentials, [], true);
            }
            if (!$token)
                throw new AccessDeniedHttpException();
        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        //------- build client response ----- //
        $client_response = [
            'response_code' => '00',
            'response_description' => 'Success',
            'token' => $token
        ];

        return response()->json($client_response);
    }

    /**
     * Check If User Email Exists
     * 
     * @return json
     */
    public function checkEmail(Response $response, Request $request) {
        $params = $request->input('params');
        $user = User::CheckEmail($params['email']);
        $status = $user->count() > 0 ? true : false;
        return response()->json(['status' => $status]);
    }

}
