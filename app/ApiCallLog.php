<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiCallLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'api_call_logs';
    
    protected $fillable = ['log_ip', 'log_method', 'log_request_original', 'log_request', 'log_response', 'log_response_sent2client', 'log_response_updated'];
}
