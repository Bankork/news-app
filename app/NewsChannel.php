<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsChannel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news_channels';

    /**
     * Mask the dates to carbon
     * 
     * @var array
     * 
     */
    protected $dates = ['created_at', 'updated_at']; 
    
    /**
     * Res the relationship between news and news channel
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function post()
    {
    	return $this->hasOne('App\NewsChannel', 'news_channel_id');
    }
    
    // relationship between channel and user channel
    public function channeluser() {
        return $this->belongsToMany('App\User','user_channels');
    }
}
