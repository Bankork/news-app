<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
   
    // relationship between user and group
    public function users() {
        return $this->hasMany('App\User','user_group_id');
    }
}
