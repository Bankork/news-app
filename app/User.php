<?php

namespace App;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Automatically creates hash for the user password.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    
    // relationship between users and user channel
    public function userchannel() {
        return $this->belongsToMany('App\NewsChannel','user_channels');
    }
    
    
    public function scopeChannel($q,$id){
        $q->with(['userchannel' => function ($query) use($id) {
                    $query->where('user_id', $id);
                }])->where('id', $id);
    }
    
    public function scopeGetChannel($q,$id,$getid=true){
        $uchannels = User::channel($id)->get(); 
//        dd($uchannels);
        $arr = [];
        foreach($uchannels as $ch){
            if ($getid) {
                foreach ($ch->userchannel as $uc) {
                    $arr[] = $uc->id;
                }
            }else{
                $arr = $ch->userchannel;
            }
        }
        return $arr;
    }
    
    // relationship between users and user channel
    public function usergroup() {
        return $this->belongsTo('App\UserGroup');
    }
    
    /**
     * Return the relationship between users and comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function post_comments()
    {
        return $this->hasMany('App\NewsComment', 'comment_by');
    }
    
    /**
     * Return the relationship between users and news likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function post_likes()
    {
        return $this->hasMany('App\NewsLike', 'liked_by');
    }
}
