<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsPost extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news_posts';

    /**
     * Mask the dates to carbon
     * 
     * @var array
     * 
     */
    protected $dates = ['created_at', 'updated_at']; //carbon objects

    /**
     * Res the relationship between news and news files
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function news_files()
    {
    	return $this->hasMany('App\NewsFile', 'news_post_id');
    }
    
    /**
     * Res the relationship between news and news files
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function news_likes()
    {
        return $this->hasMany('App\NewsLike', 'news_post_id');
    }

    /**
     * Return the relationship between news and news channel
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function nchannel()
    {
    	return $this->belongsTo('App\NewsChannel','news_channel_id');
    }
    
    /**
     * Return the relationship between news and users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * 
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
         /**
     * Res the relationship between news and news files
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function comments()
    {
        return $this->hasMany('App\NewsComment', 'news_post_id');
    }
}
