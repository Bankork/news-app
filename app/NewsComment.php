<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsComment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news_comments';
    
    /**
     * Return the relationship between news and news comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function comment_post()
    {
    	return $this->belongsTo('App\NewsComment');
    }
    
    /**
     * Res the relationship between user and news comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'comment_by');
    }
}
