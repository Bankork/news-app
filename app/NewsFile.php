<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsFile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news_files';

    /**
     * Return the relationship between news and news files
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function npost()
    {
    	return $this->belongsTo('App\NewsPost');
    }
    
    
}
