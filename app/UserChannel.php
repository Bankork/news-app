<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserChannel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_channels';

    /**
     * Mask the dates to carbon
     * 
     * @var array
     * 
     */
    protected $dates = ['created_at', 'updated_at']; //carbon objects
    
//    public function usernewschannel() {
//        return $this->hasManyThrough('App\User', 'App\NewsChannel', 'user_id', 'news_channel_id');
//    }

}
