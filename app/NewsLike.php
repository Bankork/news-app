<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsLike extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news_likes';
    
    /**
     * Return the relationship between news and news like
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function like_post()
    {
    	return $this->belongsTo('App\NewsPost');
    }
    
    /**
     * Res the relationship between user and news likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * 
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
