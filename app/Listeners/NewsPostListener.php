<?php

namespace App\Listeners;

use App\Events\NewsPostEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewsPostListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewsPostEvent  $event
     * @return void
     */
    public function handle(NewsPostEvent $event)
    {
        
    }
}
