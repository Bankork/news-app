<?php

namespace App\Classes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\User;
//use App\Post;
use App\UserFile;
//use App\Follower;
//use App\MessagesNotification;
//use App\Inquiry;
use URL;
//use Mail;
use App\Log as ActivityLog;

class Helper {

    /**
     * Return a user's profile picture
     *
     * @return  string
     * 
     */
    public function userProfileImage(Request $request) {
        // $user = $request->user()->user_profile_image->first()->id;
        $user = User::find($request->user()->id);
        $user_profile_image = $user->user_profile_image == null ? URL::asset('public/assets/img/no-profile-img.jpg') : URL::asset($user->user_profile_image->file_path);

        return $user_profile_image;
    }

    /**
     * User Posts Counter
     * 
     */
//    public function userPostsCounter($user_id) {
//        $user = Post::where('user_id', $user_id)->where('status', true)->get();
//        return $user->count();
//    }

    /**
     * Log user activity
     * 
     * @return void
     * @param $user_id integer
     * @param $activity string
     * 
     */
    public function logUserActivity($user_id, $activity) {
        $log = new ActivityLog;
        $log->user_id = $user_id;
        $log->activity = $activity;
        $log->save();
    }

    /**
     * Get the image file for a user
     * 
     * @return string
     * @param $user_id integer
     * @param $file_name string
     * @param $file_type string
     * 
     */
    public function getImageFile($user_id, $file_name, $file_type = null) {
        $file = UserFile::where('user_id', $user_id)->where('file_name', $file_name);
        if ($file_type !== null) {
            $file = $file->where('file_type', $file_type)->first();
        } else {
            $file = $file->first();
        }

        return $file ? URL::asset($file->file_path) : false;
    }

    /**
     * Return a user's mini profile picture
     *
     * @return  string
     * 
     */
    public function userMiniProfileImage(Request $request) {
        // $user = $request->user()->user_profile_image->first()->id;
        $user = User::with(['user_file' => function($query) {
                        $query->where('file_name', 'small');
                    }])->where('id', $request->user()->id)->first();
        return response()->json($user);
        $user_profile_image = $user->user_file == null ? URL::asset('public/assets/img/no-mini-profile-pic.png') : URL::asset($user->user_file->file_path);

        return $user_profile_image;
    }

    /**
     * Check Followers
     * 
     * @return boolean
     * @param \Illuminate\Http\Request
     * @param $followee_id integer
     * 
     */
//    public function isFollowing($follower_id, $followee_id) {
//        $followership = Follower::where('follower_id', $follower_id)->where('followee_id', $followee_id)->first();
//        if ($followership) {
//            return (int) 1;
//        } else {
//            return (int) 0;
//        }
//    }

    /**
     * Send out mails
     * 
     * @return void
     * @param  int $delay
     * @param string $template
     * @param array $template_data
     * @param array $messgae_data
     * 
     */
//    public function sendEmail($delay, $template, $template_data, $message_data) {
//        Mail::later($delay, $template, $template_data, function($m) use($message_data) {
//            $m->from($message_data->default_sender_email_address, $message_data->default_sender_name);
//            $m->to($message_data->recipient_email, $message_data->recipient_name)->subject($message_data->subject);
//        });
//    }

    /**
     * Reset read messages
     * 
     * @return void
     * @param int $inquiry_id
     * @param int $notification_id
     * 
     */
//    public function resetMessages($inquiry_id, $notification_id) {
//        if ($notification_id) {
//            $notification = MessagesNotification::find($notification_id);
//            if (!$notification->is_read) {
//                $notification->is_read = true;
//                $notification->save();
//            }
//        }
//    }

    /**
     * Mark as read
     * 
     * @return void
     * @param int $inquiry_id
     * 
     */
//    public function markAsRead($inquiry_id) {
//        $inquiry = Inquiry::find($inquiry_id);
//        if ($inquiry) {
//            $inquiry->is_read = true;
//            $inquiry->save();
//            //update other associated records;
//            $update_inquiry = Inquiry::where('parent_id', $inquiry_id)->update(['is_read' => true]);
//        }
//    }

    /**
     * Get Fellowership statistics
     * 
     * @return integer
     * @param $fellowership_type string
     * @param $user_id integer 
     * 
     */
//    public function followershipStatistics($fellowership_type, $user_id) {
//        switch ($fellowership_type) {
//            case 'followers':
//                $followers = Follower::GetFollowers($user_id);
//                return $followers->count();
//                break;
//
//            case 'followees':
//                $followees = Follower::GetFollowees($user_id);
//                return $followees->count();
//                break;
//        }
//    }

    /**
     * Generate Slug
     *
     * @return  string
     * @param $first_name 
     * @param $last_name
     * 
     */
    public function generateSlug($first_name, $last_name) {
        $full_name = strtolower($first_name) . ' ' . strtolower($last_name);
        $slug = str_slug($full_name);
        $user_slug = User::where('slug', $slug)->get();
        $slug = $user_slug->count() > 0 ? $slug . '-' . $this->generateRandomString(4) : $slug;

        return (string) $slug;
    }

    /**
     * Return a capitalized string
     *
     * @return  string
     * @param  $string String to capitalize
     */
    public function capitalize($string) {
        return (string) ucwords(strtolower($string));
    }

    /**
     * Check the existence of a string
     * 
     * @return boolean
     * @param string $string
     * @param string $data
     */
    public function checkData($string, $data) {
        return strpos($string, $data);
    }

    /**
     * Get a user's details
     * 
     * @return App\User
     * @param int $user_id
     */
    public function getUserDetails($user_id) {
        return User::find($user_id);
    }

    /**
     * Return a randomly generated string
     *
     * @return string
     * @param int $length --- Length of charcters
     *
     * */
    public function generateRandomString($length = 6) {
        $key = "";
        $possible = "0123456789abcdefghijkmnopqrstuvwxyz";
        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
            if (!strstr($key, $char)) {
                $key .= $char;
                $i++;
            }
        }

        //return key
        return $key;
    }

}

?>