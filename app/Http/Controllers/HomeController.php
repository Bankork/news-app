<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Default Layout
     *
     * @var string
     */
    protected $layout = 'layouts.app';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        return $this->setPageContent(view('shared.body', $data));
    }
    
    /**
     * Set Controller Default Layout  And Render Content
     * 
     * @param string $content
     * @return \illuminate\Http\Response
     */
    protected function setPageContent($content) {
        return view($this->layout, ['content' => $content]);
    }
    
}
