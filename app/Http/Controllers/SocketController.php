<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use LRedis;
//use App\Events\NewsPost;
use App\NewsPost;

class SocketController extends Controller {

    public function __construct() {
        $this->middleware('guest');
    }

    public function index() {

        return view('socket');
    }

    public function writemessage() {

        return view('writemessage');
    }

//    public function sendMessage() {
//
//        //---- save post ----- //
//        $news = new NewsPost();
//        $news->user_id = '1';
//        $news->news_channel_id = Request::input('channel');
//        $news->slug = Request::input('slug');
//        $news->post = Request::input('message');
//        $news->status = true;
//        $news->completed_post_process = true;
//        $news->save();
//        
//        $redis = LRedis::connection();
//        $redis->publish('message', Request::input('message'));
//        //fire event
//        Event::fire(new NewsPost($approved_post->toJson()));
//
//        return redirect('writemessage');
//    }

}
