@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Send message</div>
                    <form action="api/sendmessage" method="POST">
                        {{ csrf_field() }}
                        <select name="channel">
                            <option value="1">Sport</option>
                            <option value="2">Technology</option>
                            <option value="3">Entertainment</option>
                        </select>
                        <input type="text" name="slug" />
                        <textarea rows="3" name="message" ></textarea>
                        <input type="submit" value="send">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection