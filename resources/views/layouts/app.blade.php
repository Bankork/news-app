<!DOCTYPE html>
<html lang="en">

    <head>

        <title>Imagine</title>

        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Main Font -->
        <script src="js/webfontloader.min.js"></script>
        <script>
            WebFont.load({
                google: {
                    families: ['Roboto:300,400,500,700:latin']
                }
            });
        </script>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">

        <!-- Theme Styles CSS -->
        <link rel="stylesheet" type="text/css" href="css/theme-styles.css">
        <link rel="stylesheet" type="text/css" href="css/blocks.css">
        <link rel="stylesheet" type="text/css" href="css/fonts.css">

        <!-- Styles for plugins -->
        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="css/simplecalendar.css">
        <link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
        <!-- Lightbox popup script-->
        <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">

        @yield('extra_css')
        
        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>

        <!-- Fixed Sidebar Left -->

         @include('shared.left-sidebar')

        <!-- ... end Fixed Sidebar Left -->


        <!-- Fixed Sidebar Right -->

        @include('shared.right-sidebar')

        <!-- ... end Fixed Sidebar Right -->

        <!-- Header -->

        @include('shared.header')

        <!-- ... end Header -->

        

                <!-- Main Content -->

                @yield('content')

                <!-- ... end Main Content -->


                <!-- Left Sidebar -->

                @include('shared.body-left')

                <!-- ... end Left Sidebar -->


                <!-- Right Sidebar -->

                @include('shared.body-right')

                <!-- ... end Right Sidebar -->

            

        @include('shared.footer')
        
        