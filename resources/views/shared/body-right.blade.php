<!-- Right Sidebar -->

                <aside class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">

                    <div class="ui-block">
                        <div class="widget w-birthday-alert">
                            <div class="icons-block">
                                <svg class="olymp-cupcake-icon"><use xlink:href="icons/icons.svg#olymp-cupcake-icon"></use></svg>
                                <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg></a>
                            </div>

                            <div class="content">
                                <div class="author-thumb">
                                    <img src="img/avatar48-sm.jpg" alt="author">
                                </div>
                                <span>Today is</span>
                                <a href="#" class="h4 title">Marina Valentine’s Birthday!</a>
                                <p>Leave her a message with your best wishes on her profile page!</p>
                            </div>
                        </div>
                    </div>


                    <div class="ui-block">
                        <div class="widget w-action">

                            <img src="img/logo.png" alt="Olympus">
                            <div class="content">
                                <h4 class="title">IMAGINE</h4>
                                <span>THE BEST SOLUTION PROVIDER YOU CAN GET!</span>
                                <a href="#" class="btn btn-bg-secondary btn-md">Read more!</a>
                            </div>
                        </div>
                    </div>

                </aside>

                <!-- ... end Right Sidebar -->