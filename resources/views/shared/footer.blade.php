

</div>
        </div>


<!-- jQuery first, then Other JS. -->
        <script src="js/jquery-3.2.0.min.js"></script>


        <!-- Js effects for material design. + Tooltips -->
        <script src="js/material.min.js"></script>

        <!-- Helper scripts (Tabs, Equal height, Scrollbar, etc) -->
        <script src="js/theme-plugins.js"></script>

        <!-- Init functions -->
        

        <!-- Load more news AJAX script -->
        <script src="js/ajax-pagination.js"></script>

        <!-- Select / Sorting script -->
        <script src="js/selectize.min.js"></script>

        <!-- Datepicker input field script-->
        <script src="js/moment.min.js"></script>
        <script src="js/daterangepicker.min.js"></script>

        <!-- Lightbox plugin script-->
        <script src="js/jquery.magnific-popup.min.js"></script>

        <!-- Widget with events script-->
        <script src="js/simplecalendar.js"></script>


        <script src="js/mediaelement-and-player.min.js"></script>
        <script src="js/mediaelement-playlist-plugin.min.js"></script>

        @yield('extra_js_files')
    </body>

</html>