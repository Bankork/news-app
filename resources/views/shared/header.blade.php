<!-- Header -->

        <header class="header" id="site-header">

            <div class="page-title">
                <h6>Newsfeed</h6>
            </div>

            <div class="header-content-wrapper">
                <form class="search-bar w-search notification-list friend-requests">
                    <div class="form-group with-button">
                        <input class="form-control js-user-search" placeholder="Search here your news..." type="text">
                        <button>
                            <svg class="olymp-magnifying-glass-icon"><use xlink:href="icons/icons.svg#olymp-magnifying-glass-icon"></use></svg>
                        </button>
                    </div>
                </form>

                <div class="control-block">

                    <div class="control-icon more has-items">
                        <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                        <div class="label-avatar bg-purple">2</div>

                        <div class="more-dropdown more-with-triangle triangle-top-center">
                            <div class="ui-block-title ui-block-title-small">
                                <h6 class="title">Chat / Messages</h6>
                                <a href="#">Mark all as read</a>
                                <a href="#">Settings</a>
                            </div>

                            <div class="mCustomScrollbar" data-mcs-theme="dark">
                                <ul class="notification-list chat-message">
                                    <li class="message-unread">
                                        <div class="author-thumb">
                                            <img src="img/avatar59-sm.jpg" alt="author">
                                        </div>
                                        <div class="notification-event">
                                            <a href="#" class="h6 notification-friend">Diana Jameson</a>
                                            <span class="chat-message-item">Hi James! It’s Diana, I just wanted to let you know that we have to reschedule...</span>
                                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>
                                        </div>
                                        <span class="notification-icon">
                                            <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                                        </span>
                                        <div class="more">
                                            <svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="author-thumb">
                                            <img src="img/avatar60-sm.jpg" alt="author">
                                        </div>
                                        <div class="notification-event">
                                            <a href="#" class="h6 notification-friend">Jake Parker</a>
                                            <span class="chat-message-item">Great, I’ll see you tomorrow!.</span>
                                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>
                                        </div>
                                        <span class="notification-icon">
                                            <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                                        </span>

                                        <div class="more">
                                            <svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="author-thumb">
                                            <img src="img/avatar61-sm.jpg" alt="author">
                                        </div>
                                        <div class="notification-event">
                                            <a href="#" class="h6 notification-friend">Elaine Dreyfuss</a>
                                            <span class="chat-message-item">We’ll have to check that at the office and see if the client is on board with...</span>
                                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 9:56pm</time></span>
                                        </div>
                                        <span class="notification-icon">
                                            <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                                        </span>
                                        <div class="more">
                                            <svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg>
                                        </div>
                                    </li>

                                </ul>
                            </div>

                            <a href="#" class="view-all bg-purple">View All Messages</a>
                        </div>
                    </div>

                    <div class="author-page author vcard inline-items more">
                        <div class="author-thumb">
                            <img alt="author" src="img/author-page.jpg" class="avatar">
                            <span class="icon-status online"></span>
                            <div class="more-dropdown more-with-triangle">
                                <div class="ui-block-title ui-block-title-small">
                                    <h6 class="title">Your Account</h6>
                                </div>

                                <ul class="account-settings">
                                    <li>
                                        <a href="#">

                                            <svg class="olymp-menu-icon"><use xlink:href="icons/icons.svg#olymp-menu-icon"></use></svg>

                                            <span>Profile Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <svg class="olymp-logout-icon"><use xlink:href="icons/icons.svg#olymp-logout-icon"></use></svg>

                                            <span>Log Out</span>
                                        </a>
                                    </li>
                                </ul>

                           </div>
                        </div>
                        <a href="#" class="author-name fn">
                            <div class="author-title">
                                James Spiegel <svg class="olymp-dropdown-arrow-icon"><use xlink:href="icons/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
                            </div>
                            <span class="author-subtitle">SPACE COWBOY</span>
                        </a>
                    </div>

                </div>
            </div>

        </header>

        <!-- ... end Header -->


        <!-- Responsive Header -->

        <header class="header header-responsive" id="site-header-responsive">

            <div class="header-content-wrapper">
                <ul class="nav nav-tabs mobile-app-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#chat" role="tab">
                            <div class="control-icon has-items">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                                <div class="label-avatar bg-purple">2</div>
                            </div>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#search" role="tab">
                            <svg class="olymp-magnifying-glass-icon"><use xlink:href="icons/icons.svg#olymp-magnifying-glass-icon"></use></svg>
                            <svg class="olymp-close-icon"><use xlink:href="icons/icons.svg#olymp-close-icon"></use></svg>
                        </a>
                    </li>
                </ul>
            </div>

            <!-- Tab panes -->
            <div class="tab-content tab-content-responsive">

               <div class="tab-pane " id="chat" role="tabpanel">

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <div class="ui-block-title ui-block-title-small">
                            <h6 class="title">Chat / Messages</h6>
                            <a href="#">Mark all as read</a>
                            <a href="#">Settings</a>
                        </div>

                        <ul class="notification-list chat-message">
                            <li class="message-unread">
                                <div class="author-thumb">
                                    <img src="img/avatar59-sm.jpg" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">Diana Jameson</a>
                                    <span class="chat-message-item">Hi James! It’s Diana, I just wanted to let you know that we have to reschedule...</span>
                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>
                                </div>
                                <span class="notification-icon">
                                    <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                                </span>
                                <div class="more">
                                    <svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg>
                                </div>
                            </li>

                            <li>
                                <div class="author-thumb">
                                    <img src="img/avatar60-sm.jpg" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">Jake Parker</a>
                                    <span class="chat-message-item">Great, I’ll see you tomorrow!.</span>
                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>
                                </div>
                                <span class="notification-icon">
                                    <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                                </span>

                                <div class="more">
                                    <svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg>
                                </div>
                            </li>
                            <li>
                                <div class="author-thumb">
                                    <img src="img/avatar61-sm.jpg" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">Elaine Dreyfuss</a>
                                    <span class="chat-message-item">We’ll have to check that at the office and see if the client is on board with...</span>
                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 9:56pm</time></span>
                                </div>
                                <span class="notification-icon">
                                    <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                                </span>
                                <div class="more">
                                    <svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg>
                                </div>
                            </li>

                            <li class="chat-group">
                                <div class="author-thumb">
                                    <img src="img/avatar11-sm.jpg" alt="author">
                                    <img src="img/avatar12-sm.jpg" alt="author">
                                    <img src="img/avatar13-sm.jpg" alt="author">
                                    <img src="img/avatar10-sm.jpg" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">You, Faye, Ed &amp; Jet +3</a>
                                    <span class="last-message-author">Ed:</span>
                                    <span class="chat-message-item">Yeah! Seems fine by me!</span>
                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 16th at 10:23am</time></span>
                                </div>
                                <span class="notification-icon">
                                    <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                                </span>
                                <div class="more">
                                    <svg class="olymp-three-dots-icon"><use xlink:href="icons/icons.svg#olymp-three-dots-icon"></use></svg>
                                </div>
                            </li>
                        </ul>

                        <a href="#" class="view-all bg-purple">View All Messages</a>
                    </div>

                </div>

                <div class="tab-pane " id="search" role="tabpanel">

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <form class="search-bar w-search notification-list friend-requests">
                            <div class="form-group with-button">
                                <input class="form-control js-user-search" placeholder="Search your news..." type="text">
                            </div>
                        </form>
                    </div>

                </div>

            </div>
            <!-- ... end  Tab panes -->

        </header>

        <!-- ... end Responsive Header -->

        <div class="header-spacer"></div>
        
        <div class="container">
            <div class="row">