<!-- Fixed Sidebar Right -->

        <div class="fixed-sidebar right">
            <div class="fixed-sidebar-right sidebar--small" id="sidebar-right">

                <div class="mCustomScrollbar" data-mcs-theme="dark">
                    <ul class="chat-users">
                        <li class="inline-items">
                            <div class="author-thumb">
                                <img alt="author" src="img/avatar67-sm.jpg" class="avatar">
                                <span class="icon-status online"></span>
                            </div>
                        </li>
                        <li class="inline-items">
                            <div class="author-thumb">
                                <img alt="author" src="img/avatar62-sm.jpg" class="avatar">
                                <span class="icon-status online"></span>
                            </div>
                        </li>

                    </ul>
                </div>

                <div class="search-friend inline-items">
                    <a href="#" class="js-sidebar-open">
                        <svg class="olymp-menu-icon"><use xlink:href="icons/icons.svg#olymp-menu-icon"></use></svg>
                    </a>
                </div>

                <a href="#" class="olympus-chat inline-items">
                    <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                </a>

            </div>

            <div class="fixed-sidebar-right sidebar--large" id="sidebar-right-1">

                <div class="mCustomScrollbar" data-mcs-theme="dark">

                    <div class="ui-block-title ui-block-title-small">
                        <a href="#" class="title">Close Friends</a>
                        <a href="#">Settings</a>
                    </div>

                 </div>

            </div>
        </div>

        <!-- ... end Fixed Sidebar Right -->

        <!-- Fixed Sidebar Right -->

        <div class="fixed-sidebar right fixed-sidebar-responsive">

            <div class="fixed-sidebar-right sidebar--small" id="sidebar-right-responsive">

                <a href="#" class="olympus-chat inline-items js-chat-open">
                    <svg class="olymp-chat---messages-icon"><use xlink:href="icons/icons.svg#olymp-chat---messages-icon"></use></svg>
                </a>

            </div>

        </div>

        <!-- ... end Fixed Sidebar Right -->