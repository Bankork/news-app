<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiCallLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_call_logs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
             $table->string('log_ip', 20);
            $table->string('log_method', 50);
            //$table->integer('log_client_id')->unsigned()->default('0');
            $table->integer('log_client_id')->index();
            //$table->foreign('log_client_id')->references('id')->on('users');
            $table->longText('log_request_original');
            $table->longText('log_request');
            $table->longText('log_response');
            $table->longText('log_response_sent2client');
            $table->enum('log_response_updated', ['0', '1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_call_logs');
    }
}
