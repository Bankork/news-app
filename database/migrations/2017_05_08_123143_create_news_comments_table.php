<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_comments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('news_post_id')->unsigned();
            $table->foreign('news_post_id')->references('id')->on('news_posts')->onDelete('cascade');
            $table->integer('comment_by')->unsigned();
            $table->foreign('comment_by')->references('id')->on('users')->onDelete('cascade');
            $table->text('comment');
            $table->tinyinteger('status')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_comments');
    }
}
