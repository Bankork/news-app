<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to this item is only for authenticated user. Provide a token in your request!'
            ]);
        });
        
        $api->group(['prefix' => 'imagine'], function (Router $api) {
//		$api->resource('news', 'App\\Api\\V1\\Controllers\\ImagineController');
		$api->post('news', 'App\\Api\\V1\\Controllers\\ImagineController@news');
	});

        $api->get('test', 'App\\Api\\V1\\Controllers\\ImagineController@testinsert'); 
        
        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'Welcome to Imagine News App APIs.'
        ]);
    });
    
    $api->post('gentoken', 'App\\Api\\V1\\Controllers\\ImagineController@generateToken');
    $api->get('post/{id}', 'App\\Api\\V1\\Controllers\\ImagineController@post');
    $api->get('updatepost/{id}', 'App\\Api\\V1\\Controllers\\ImagineController@updatepost');
    $api->post('sendmessage', 'App\\Api\\V1\\Controllers\\ImagineController@sendMessage');
    $api->get('mychannel', 'App\\Api\\V1\\Controllers\\ImagineController@mychannel');
});
